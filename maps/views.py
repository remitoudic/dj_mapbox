from django.shortcuts import render


def default_map(request):
    return render(request, 'default.html', {})
# Create your views here.
def default_map(request):
    # TODO: move this token to Django settings from an environment variable
    # found in the Mapbox account settings and getting started instructions
    # see https://www.mapbox.com/account/ under the "Access tokens" section
    mapbox_access_token = 'pk.eyJ1IjoicmVtaXRvdWRpYyIsImEiOiJjam8xcTRqZWUwOTVkM2txbjZlcWU2eXZtIn0.asbHu3g_m0dDF3NWqJ5QBg'
    return render(request, 'default.html',
                  { 'mapbox_access_token': mapbox_access_token })
